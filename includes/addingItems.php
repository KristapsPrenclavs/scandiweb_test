<?php 

//Nav kopā ar pārējām metodēm, jo netieku galā ar informācijas nodošanu caur <form>, man tā liekas.
    try
    {
        $pdo = new PDO("mysql:host=localhost;dbname=shoppingcart", "root", "pimpausis");
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } 
    catch(PDOException $e)
    {
        die("ERROR: " . $e->getMessage());
    }
        //Tabulu nosaukumi datubāzē
        $tablename = $_POST['tablename']; 

    try
    {      
        $sql = ("INSERT INTO  $tablename (id, name, price, special) VALUES (:id, :name, :price, :special)");
        $stmt = $pdo->prepare($sql);
        
        // vērtību bindings
        $stmt->bindParam(':id', $_REQUEST['id']);
        $stmt->bindParam(':name', $_REQUEST['name']);
        $stmt->bindParam(':price', $_REQUEST['price']);
        $stmt->bindParam(':special', $_REQUEST['special']);
        

        $stmt->execute();
        echo "Great success!";
    } 
    catch(PDOException $e)
    {
        die("ERROR: $sql. " . $e->getMessage());
    }
    unset($pdo);
?>
function showChairs() {
    var typeValue = document.getElementById("typeValue");
    var divChair = document.getElementById("divChair");
    divChair.style.display = typeValue.value == "chair" ? "block" : "none";
}

function showBooks() {
    var typeValue = document.getElementById("typeValue");
    var divBooks = document.getElementById("divBooks");
    divBooks.style.display = typeValue.value == "books" ? "block" : "none";
}

function showCd() {
    var typeValue = document.getElementById("typeValue");
    var divCd = document.getElementById("divCd");
    divCd.style.display = typeValue.value == "cd" ? "block" : "none";
}
<!--No sākuma izlasīju Product Add kā Product Ad page, tāpēc noklikšķinot uz 'produktiem' tie atveras jaunālapā (cerams ka drīkstēju atstāt).
    Savukārt produktu dinamisku dzēšanu man nesanāk uztaisīt.
    Šis ir mans primais web projekts, lūdzu esiet saprotoši :). Mootivācija mācīties ir liela, taču vajag nelielu grūdienu. Paldies.
  -->

<?php 
include 'db.php';
include 'methods.php'; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Scandiweb_test</title>
</head>
<body>      
    <header id="header">
        <div id="header-text">
            <h1>Product List </h1>
        </div>
        <div class="header-btn">          
            <a href="addItem.php" class="btn btn-info" role="button">Add Item</a>          
        </div>
    </header>
<!-- principā visa preču sadaļa ir viena liela forma -->
<form  method="post"> 
<div class="header-btn">
    <button type="submit" name="delete" >Delete Selected</button>
</div>

        <div class="frame">
            <div class="container1">            
                <?php  $chair = new User; $chair -> getChairs();?>
            </div> 
            
            <div class="container2">              
                <?php  $book = new User; $book -> getBooks();?>              
            </div> 
            
            <div class="container3">              
                <?php  $cd = new User; $cd -> getCd();?>
            </div> 
        </div>
</form>

<footer id="main-footer" class="grid">
    <div id="footer-title">Junior Test For Scandiweb</div>
    <div id="contact">Made By Kristaps Prenclavs. Contact me via - <a href="https:/gmail.com" target="_blank">kristaps.prenclavs@gmail.com</a></div>
</footer>

</body>
</html>
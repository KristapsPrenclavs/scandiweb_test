<?php  include 'db.php'; include 'methods.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<header><h1>Add Products</h1></header>


<!-- Uz klikšķa parāda īsto <div> -->
<script type="text/javascript">
    <?php include 'includes/showItems.js' ?>
</script>
<body>
<select id = "typeValue" onchange = "showChairs(); showBooks(); showCd()">
    <option value="">Type Switcher</option>
    <option value="chair">Chairs</option>
    <option value="books">Books</option>
    <option value="cd">CD's</option>
</select>
 
<?php include 'includes/showChairs.php' ?> 
<?php include 'includes/showBooks.php' ?>
<?php include 'includes/showCds.php' ?>

<a href="index.php">Back to site</a>
    


</body>
</html>